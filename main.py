#command "pip install -r requirements.txt" voor het installeren van de nodige dependencies
# pip install -r requirements.txt

import socket
import sys
import os
from pythonping import ping

antwCheck = 0

hostname = socket.gethostname()
print("De hostname van dit systeem is: ", hostname)

ipAdress = socket.gethostbyname(hostname)
print("Het IP-adres van dit systeem is: ", ipAdress)

ipv6Adress = socket.getaddrinfo("example.com", None, socket.AF_INET6)
print("Het IPv6-adres van dit systeem is: ", ipv6Adress)
print("")

while antwCheck == 0:
    YesOrNo1 = input("Wil je je internetverbinding testen? Y/N ")
    if YesOrNo1 == "Y":
        print("")
        ping('google.com', verbose=True)
        print("")
        print('Op basis van bovenstaande replies and latency daarvan kun je concluderen of je internetverbinding stabiel is')
        antwCheck = 1

    if YesOrNo1 == "N":
        exit()
